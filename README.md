# Api Proxy

Nginx proxy app for our Django app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen pon (default: `8000`)
* `APP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward request to (default: `9000`)
